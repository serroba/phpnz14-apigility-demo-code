<?php

namespace PropertyApp\V1\Rest\Property;

use Zend\ServiceManager\ServiceLocatorInterface;

class ResourceFactory
{
    public function __invoke(ServiceLocatorInterface $services)
    {
        $resource = new Resource();
        $resource->setMapper($services->get('PropertyMapper'));
        return $resource;
    }
}
